# Installing Postchain
This document covers installation of the Postchain consortium database and a configuration of the Postchain exchangeable tokens (ETK) application.
## Prerequisites

[The install options don't seem to include normal Linux install, e.g. apt]
1.  **Java 8**
    * Postchain runs on [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).
    * Make sure `java -version` displays Java 8 as your version.
2. **PostgreSQL**
    * Install [PostgreSQL](https://www.postgresql.org/download/).
3. **Maven**
    * Install [Apache Maven](https://maven.apache.org/install.html).
    * Add Maven to your system's `PATH` environment variable, per installation instructions. [this would be done automatically by package managers such as apt]
    * Verify that `mvn -v` displays your configuration.
## Build

Download the Postchain source code:

`git clone https://bitbucket.org/chromawallet/postchain2.git`

Set up PostgreSQL for Postchain:
[these intructions are for Linux it seems, might be good to indicate, e.g. by saying all instructions are for Linux, and maybe also Mac?]
```
sudo -u _postgres -i
createdb postchain
psql -c "create role postchain LOGIN ENCRYPTED PASSWORD 'postchain'" -d postchain
psql -c "grant ALL ON DATABASE postchain TO postchain" -d postchain
exit
```

Install Postchain with Maven:
```
cd postchain2
mvn clean install
```
You have now installed Postchain. See below for an example application.

# Postchain ETK Application
[it seems to me that you do not need to install postchain to do this]
Below is an example of how to run a single Postchain ETK node.
## Prerequisites
1. Postchain installed, per instructions above.
## Build
Download the Postchain ETK source code:

`git clone https://bitbucket.org/chromawallet/postchain2-examples.git`

Install with Maven:
```
cd postchain2-examples
mvn clean install
```
## Run
To run a Postchain ETK node, create a new directory and enter it in your console. [explain what a single node postchain is]

Copy the node properties file (NB: replace `/path/to/`):

`cp /path/to/postchain2-examples/src/test/resources/etk-single.properties .`


Copy postchain2-examples jar file:

`cp /path/to/postchain2-examples/target/postchain2-examples-1.0-SNAPSHOT.jar .`


Then, extract the following file (NB: use any extraction method):

`tar xvzf /path/to/postchain2/postchain-base/target/postchain-base-1.0-SNAPSHOT-dist.tar.gz`


From the extracted folder, copy `postchain.sh` and `postchain-base-1.0-SNAPSHOT-jar-with-dependencies.jar` into the directory you created earlier.

Finally, run the Postchain node:

`./postchain.sh -j postchain2-examples-1.0-SNAPSHOT.jar -i 0 -c etk-single.properties`
